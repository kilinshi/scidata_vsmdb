function [sig_radar, sig_pcg, locsR, locsT] = cutSignalsAndLocs(sig_radar, sig_pcg, locsR, locsT, begin_cut, end_cut)
% function [sig_radar, sig_pcg, locsR, locsT] = cutSignalsAndLocs(sig_radar, sig_pcg, locsR, locsT, begin_cut, end_cut)
% cut the two signals and location vectors to begin_cut and end_cut
% input: sig_radar, sig_pcg, locations R, locations T-end, begin_cut, end_cut
% output: cut sig_radar, sig_pcg, locsR, locsT
%% Authors: Kilin Shi, Sven Schellenberger
% Copyright (C) 2017  Kilin Shi, Sven Schellenberger

if end_cut == -1
    if length(sig_radar) > length(sig_pcg)
        end_cut_global = length(sig_pcg);
    else
        end_cut_global = length(sig_radar);
    end
else
    if end_cut > length(sig_pcg)
        end_cut_global = length(sig_pcg);
    else
        end_cut_global = end_cut;
    end
end

begin_cut_global = begin_cut;

% Cut ECG locs
locsR = locsR(locsR > begin_cut_global);
locsR = locsR(locsR < end_cut_global);
locsR = locsR - begin_cut_global + 1;

locsT = locsT(locsT > begin_cut_global);
locsT = locsT(locsT < end_cut_global);
locsT = locsT - begin_cut_global + 1;

% Cut signals
sig_radar = sig_radar(begin_cut_global:end_cut_global);
sig_pcg = sig_pcg(begin_cut_global:end_cut_global);

end

