function [abl, locsR, locsT] = getEcgLocs(pathtofolders, timestamp)
% function [abl, locsR, locsT] = getEcgLocs(pathtofolders, timestamp)
% load the locations for r-peaks and t-waveends from peakfile
% input: path to datasets, timestamp
% output: lead#, R-peak locations, T-waveend locations
%% Authors: Kilin Shi, Sven Schellenberger
% Copyright (C) 2017  Kilin Shi, Sven Schellenberger

peakfile = findFile(pathtofolders, ['Peak_', timestamp]);
fileID = fopen(peakfile,'r');
data = textscan(fileID,'%s %s', 'Delimiter', ';');

fclose(fileID);
if isempty(str2num(data{1}{2,1}))
    error('Peakfile not edited yet');
else
    fileID = fopen(peakfile,'r');
    header = textscan(fileID,'%s %s', 1,'delimiter',';');
    data = textscan(fileID,'%f %f', 'Delimiter', ';');
    fclose(fileID);
end

%% get data from vectors
if length(header) == 2
    temp = cell2mat(header{1});
    abl = str2num(temp(end));
    locsR = cell2mat(data(1));
    locsR = locsR(locsR > 0);
    locsT = cell2mat(data(2));
    locsT = locsT(locsT > 0);
else
    error('too much header');
end



